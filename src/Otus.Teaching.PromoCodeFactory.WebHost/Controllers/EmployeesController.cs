﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IRepository<Role> _rolesRepository;

        public EmployeesController(IEmployeeRepository employeeRepository, IRepository<Role> rolesRepository)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создать сотрудника
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> CreateEmployeeAsync([FromBody]CreateEmployeeRequest createEmployee)
        {
            
            var employeeAlreadyExists = (await _employeeRepository.GetByEmailAsync(createEmployee.Email)) != null;

            if (employeeAlreadyExists)
                return BadRequest("Employee with the same email already exists.");

            var employeeRoles = new List<Role>();

            foreach (var r in createEmployee.Roles)
            {
                var role = (await _rolesRepository.FindAsync(role => role.Name == r)).FirstOrDefault();

                if (role == null)
                    return BadRequest($"Role with name {r} not exists");
                else
                    employeeRoles.Add(role);
            }

            var newEmployee = new Employee()
            {
                Email = createEmployee.Email,
                FirstName = createEmployee.FirstName,
                LastName = createEmployee.LastName,
                Roles = employeeRoles
            };

            newEmployee = await _employeeRepository.CreateAsync(newEmployee);

            var employeeModel = new EmployeeResponse()
            {
                Id = newEmployee.Id,
                Email = newEmployee.Email,
                Roles = newEmployee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = newEmployee.FullName,
                AppliedPromocodesCount = newEmployee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Редактировать сотрудника
        /// </summary>
        [HttpPut]
        public async Task<ActionResult<EmployeeResponse>> UpdateEmployeeAsync([FromBody] UpdateEmployeeRequest updateEmployee)
        {

            var employee = await _employeeRepository.GetByIdAsync(updateEmployee.Id);

            if (employee == null)
                return NotFound();

            if (updateEmployee.Email != employee.Email)
            {
                var employeeAlreadyExists = (await _employeeRepository.GetByEmailAsync(updateEmployee.Email)) != null;

                if (employeeAlreadyExists)
                    return BadRequest("Employee with the same email already exists.");
            }

            var employeeRoles = new List<Role>();

            foreach (var r in updateEmployee.Roles)
            {
                var role = (await _rolesRepository.FindAsync(role => role.Name == r)).FirstOrDefault();
                var roleExists = role != null;

                if (!roleExists)
                    return BadRequest($"Role with name {r} not exists");
                else
                    employeeRoles.Add(role);
            }

            employee.Email = updateEmployee.Email;
            employee.FirstName = updateEmployee.FirstName;
            employee.LastName = updateEmployee.LastName;
            employee.Roles = employeeRoles;

            employee = await _employeeRepository.UpdateAsync(employee);

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployeeAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            await _employeeRepository.DeleteAsync(employee);

            return Ok();
        }


    }
}