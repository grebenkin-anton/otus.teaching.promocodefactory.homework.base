﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EmployeeRepository : InMemoryRepository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(IEnumerable<Employee> employees) : base(employees)
        {

        }

        public Task<Employee> GetByEmailAsync(string email)
        {
            return Task.FromResult(Data.Where(e => e.Email == email).FirstOrDefault());
        }
    }
}
