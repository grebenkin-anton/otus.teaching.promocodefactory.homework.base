﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        private IEnumerable<T> _data;

        protected IEnumerable<T> Data
        {
            get
            {
                //ничего лучше не придумал
                lock (_locker)
                {
                    return _data.Select(e => e);
                }
            }
            set
            {
                lock (_locker)
                {
                    _data = value;
                }
            }
        }
        private object _locker;

        public InMemoryRepository(IEnumerable<T> data)
        {
            _locker = new object();
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<IEnumerable<T>> FindAsync(Func<T, bool> expression)
        {
            return Task.FromResult(Data.Where(expression));
        }

        public Task<T> CreateAsync(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(T));

            if (entity.Id == Guid.Empty)
                entity.Id = Guid.NewGuid();

            return Task.Run(() =>
            {
                Data = Data.Append(entity);   
                return Data.Where(e => e.Id == entity.Id).FirstOrDefault();
            });
           
        }

        public Task<T> UpdateAsync(T entity)
        {
            if (entity.Id == Guid.Empty)
                throw new ArgumentOutOfRangeException(nameof(T), "Id cannot be empty");

            return Task.Run(() =>
            {
                if (Data.Where(e => e.Id == entity.Id).Any())
                    Data = Data.Where(e => e.Id != entity.Id).Append(entity);

                return Data.Where(e => e.Id == entity.Id).FirstOrDefault();
            });
        }

        public Task DeleteAsync(T entity)
        {
            return Task.Run(() =>
            {
                Data = Data.Where(e => e.Id != entity.Id);
            });
        }
    }
}